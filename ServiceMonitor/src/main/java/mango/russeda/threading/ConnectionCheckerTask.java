package mango.russeda.threading;

import mango.russeda.connectionchecker.ConnectionChecker;

public class ConnectionCheckerTask implements Runnable {

    ConnectionChecker connectionChecker;

    public ConnectionCheckerTask(ConnectionChecker checker) {
        this.connectionChecker = checker;
    }

    public void run() {
        connectionChecker.checkConnection();
    }

    public int getPollingIntervalInMS() {
        return connectionChecker.getPollingIntervalInMS();
    }
}
