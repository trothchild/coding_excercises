package mango.russeda.threading;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

public class ConnectionCheckerScheduler {

    private List<ConnectionCheckerTask> connectionCheckerTaskList;
    private ThreadPoolTaskScheduler taskScheduler;

    public ConnectionCheckerScheduler(List<ConnectionCheckerTask> checkerTasks,
            ApplicationContext context) {
        checkParameters(checkerTasks, context);
        connectionCheckerTaskList = checkerTasks;

        taskScheduler = (ThreadPoolTaskScheduler) context
                .getBean("taskScheduler");
    }

    private void checkParameters(List<ConnectionCheckerTask> checkerTasks,
            ApplicationContext context2) {

        if (checkerTasks == null) {
            throw new IllegalArgumentException(
                    "The check connection task array should not be null");
        }

        if (context2 == null) {
            throw new IllegalArgumentException(
                    "The spring context should not be  null");
        }
    }

    public void startScheduledTasks() {

        for (ConnectionCheckerTask checkerTask : connectionCheckerTaskList) {
            taskScheduler.scheduleAtFixedRate(checkerTask,
                    checkerTask.getPollingIntervalInMS());
        }
    }
}
