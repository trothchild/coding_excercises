package mango.russeda.filehandling;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import mango.russeda.connectionchecker.ConnectionChecker;
import mango.russeda.connectionchecker.ConnectionCheckerFactory;
import mango.russeda.connectionchecker.MalformedEndPointException;
import mango.russeda.output.Output;
import mango.russeda.threading.ConnectionCheckerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoadServicesToCheckFromFile {

    public static final String DEFAULT_DELIMITER = ",";
    private File inputFile;
    private List<ConnectionCheckerTask> connectionCheckerTaskList;
    private Output outputter;
    private boolean loadFileSuccess = true;
    private final Logger logger = LoggerFactory
            .getLogger(LoadServicesToCheckFromFile.class);

    public LoadServicesToCheckFromFile(String fileName, Output outputter)
            throws NumberFormatException, FileNotFoundException, IOException,
            MalformedEndPointException {
        checkParameters(fileName, outputter);
        this.outputter = outputter;
        connectionCheckerTaskList = new ArrayList<ConnectionCheckerTask>();
        loadFile(fileName);
        if (!loadFileSuccess) {
            throw new IllegalArgumentException(
                    "Unable to load file, please check logs");
        }
    }

    private void checkParameters(String fileName, Output outputterToUse)
            throws FileNotFoundException {

        if (outputterToUse == null) {
            logger.error("Outputter can not be null");
            throw new IllegalArgumentException("Outputter can not be null");
        }
        if (fileName == null) {
            logger.error("Filename can not be null");
            throw new IllegalArgumentException("Filename can not be null");
        }

        if (fileName.equals("")) {
            logger.error("Filename can not be empty");
            throw new IllegalArgumentException("Filename can not be empty");
        }

        inputFile = new File(fileName);

        if (!inputFile.exists()) {
            logger.error("File not found [" + fileName + "]");
            throw new FileNotFoundException("File not found [" + fileName + "]");
        } else if (inputFile.isDirectory()) {
            throw new IllegalArgumentException("Filename is a directory ["
                    + fileName + "]");
        }

        if (!inputFile.canRead()) {
            logger.error("Unable to read file [" + fileName + "]");
            throw new IllegalArgumentException("Unable to read file ["
                    + fileName + "]");
        }

    }

    private void loadFile(String inputFile) throws FileNotFoundException,
            IOException, NumberFormatException, MalformedEndPointException {
        String currentLine;
        ConnectionChecker currentChecker;
        ConnectionCheckerTask currentCheckerTask;

        logger.info("loading file from " + inputFile);
        try (BufferedReader br = new BufferedReader((new FileReader(inputFile)))) {

            while ((currentLine = br.readLine()) != null)
                // parse a line
                if (currentLine != "") {
                    try {
                        currentChecker = parseAndCreateChecker(currentLine);
                        currentCheckerTask = new ConnectionCheckerTask(
                                currentChecker);
                        connectionCheckerTaskList.add(currentCheckerTask);

                    } catch (Exception e) {
                        logger.error("Unable to parse line from file ["
                                + currentLine + "] " + e.getStackTrace());
                        loadFileSuccess = false;
                    }

                } else {
                    logger.info("Ignoring blank line in load file");
                }

        }
    }

    private ConnectionChecker parseAndCreateChecker(String currentLine)
            throws NumberFormatException, MalformedEndPointException {
        String[] parameters = parseString(currentLine);
        return ConnectionCheckerFactory.createConnectionChecker(parameters[0],
                parameters[1], new Integer(parameters[2]), outputter);
    }

    static private String[] parseString(String stringToParse) {
        String[] parsedString = stringToParse.split("[" + DEFAULT_DELIMITER
                + "]");
        trim(parsedString);
        checkParsedCorrectly(parsedString);
        return parsedString;
    }

    private static void trim(String[] parsedString) {
        for (int i = 0; i < parsedString.length; i++) {
            parsedString[i] = parsedString[i].trim();
        }
    }

    private static void checkParsedCorrectly(String[] parsedString) {

        if (parsedString.length < 3) {
            throw new IllegalArgumentException("Not enough parameters");
        }

        if (parsedString.length > 3) {
            // log ignoring additional parameters
        }

        if (parsedString[0] == null || parsedString[0].equals("")) {
            throw new IllegalArgumentException(
                    "Name can not be null or zero length");
        }

        if (parsedString[1] == null || parsedString[1].equals("")) {
            throw new IllegalArgumentException(
                    "End point can not be null or zero length");
        }

        if (parsedString[2] == null || parsedString[1].equals("")) {
            throw new IllegalArgumentException("Polling interval must be set");
        }
    }

    public List<ConnectionCheckerTask> getConnectionCheckerTaskList() {
        return connectionCheckerTaskList;
    }
}
