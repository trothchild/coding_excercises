package mango.russeda.connectionchecker;

public class MalformedEndPointException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 8191861434912663182L;

    public MalformedEndPointException(Throwable cause) {
        super(cause);
    }

}
