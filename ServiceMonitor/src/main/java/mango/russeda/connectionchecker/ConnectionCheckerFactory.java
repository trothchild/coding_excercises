package mango.russeda.connectionchecker;

import mango.russeda.output.Output;

public class ConnectionCheckerFactory {

    private static enum SUPPORTED_PROTOCOLS {
        HTTP, UNSUPPORTED
    }

    private static int DEFAULT_TIME_OUT = 4000;

    public static ConnectionChecker createConnectionChecker(String name,
            String endPoint, int pollingIntervalInMS, Output outputter)
            throws MalformedEndPointException {

        checkParameters(name, endPoint, pollingIntervalInMS);

        SUPPORTED_PROTOCOLS protocol = determineProtocol(endPoint);

        switch (protocol) {
        case HTTP:
            // create a HTTPConnectionChecker
            return new HTTPConnectionChecker(name, endPoint,
                    pollingIntervalInMS, DEFAULT_TIME_OUT, outputter);
        case UNSUPPORTED:
        default:
            throw new IllegalArgumentException("Protocol is unsupported ["
                    + endPoint + "]");
        }
    }

    private static void checkParameters(String name, String endPoint,
            int pollingIntervalInMS) {

        if (name == null || endPoint == null) {
            throw new NullPointerException(
                    "The name and URL of the connection checker must be populated");
        }

        if (name == "") {
            throw new IllegalArgumentException(
                    "The name used in connection checker must be set");
        }

        if (endPoint == "") {
            throw new IllegalArgumentException(
                    "The end point used in connection checker must be set");
        }

        if (pollingIntervalInMS <= 0) {
            throw new IllegalArgumentException(
                    "The polling interval must be greater than zero ["
                            + pollingIntervalInMS + "]");
        }
    }

    private static SUPPORTED_PROTOCOLS determineProtocol(String endPoint) {

        if (checkForHTTPProtocol(endPoint)) {
            return SUPPORTED_PROTOCOLS.HTTP;
        } else {
            return SUPPORTED_PROTOCOLS.UNSUPPORTED;
        }
    }

    private static boolean checkForHTTPProtocol(String endPoint) {
        // Only match the protocol header, let URL creating check for malformed
        // URLs
        String regex = "^http://.*";

        return endPoint.matches(regex);
    }
}
