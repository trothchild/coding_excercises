package mango.russeda.connectionchecker;

public interface ConnectionChecker {
    public void checkConnection();

    public int getPollingIntervalInMS();
}
