package mango.russeda.connectionchecker;

import mango.russeda.output.Output;

public abstract class AbstractConnectionChecker implements ConnectionChecker {

    protected final String name;
    protected final String endPoint;
    protected final int pollingIntervalInMS;
    protected final int timeOutInMS;
    protected final Output outputter;

    AbstractConnectionChecker(String name, String endPoint,
            int pollingIntervalInMS, int timeOutInMS, Output outputter)
            throws MalformedEndPointException {

        checkParameters(name, endPoint, pollingIntervalInMS, timeOutInMS,
                outputter);
        this.name = name;
        this.endPoint = endPoint;
        this.pollingIntervalInMS = pollingIntervalInMS;
        this.timeOutInMS = timeOutInMS;
        this.outputter = outputter;
        ;
    }

    private void checkParameters(String name, String endPoint,
            int pollingIntervalInMS, int timeOutInMS, Output outputter)
            throws MalformedEndPointException {

        if (outputter == null) {
            throw new IllegalArgumentException(
                    "Can not create a HTTPConnectionChecker with a null output");
        }

    }

}
