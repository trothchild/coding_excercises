package mango.russeda.connectionchecker;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import mango.russeda.output.Output;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HTTPConnectionChecker extends AbstractConnectionChecker {

    private final Logger logger = LoggerFactory
            .getLogger(HTTPConnectionChecker.class);
    private URL url;
    private static final Set<Integer> VALID_RESPONSE_CODES = new HashSet<Integer>(
            Arrays.asList(200));

    HTTPConnectionChecker(String name, String endPoint,
            int pollingIntervalInMS, int timeOutInMS, Output outputter)
            throws MalformedEndPointException {
        super(name, endPoint, pollingIntervalInMS, timeOutInMS, outputter);
        checkEndPointValidURL(endPoint);
    }

    private void checkEndPointValidURL(String endPoint)
            throws MalformedEndPointException {
        try {
            url = new URL(endPoint);
        } catch (MalformedURLException e) {
            throw new MalformedEndPointException(e.getCause());
        }
    }

    HTTPConnectionChecker(String name, String endPoint,
            int pollingIntervalInMS, int timeOutInMS, Output outputter, URL url)
            throws MalformedEndPointException {
        super(name, endPoint, pollingIntervalInMS, timeOutInMS, outputter);

        this.url = url;
    }

    public void checkConnection() {
        HttpURLConnection connection;
        int responseCode;
        String responseMessage;

        try {
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("HEAD");
            connection.setConnectTimeout(timeOutInMS);
            connection.setReadTimeout(timeOutInMS);

            responseCode = connection.getResponseCode();

            if (!VALID_RESPONSE_CODES.contains(responseCode)) {
                responseMessage = "An unsuccessful response[" + responseCode
                        + "] was received from end point [" + url + "]";
                logger.warn(responseMessage);
                outputter.outputMessage(responseMessage);
            } else {
                responseMessage = "An successful response[" + responseCode
                        + "] was received from end point [" + url + "]";
                logger.debug(responseMessage);
            }

        } catch (java.net.SocketTimeoutException e) {
            responseMessage = "A response from [" + url
                    + "] was not received in time [" + timeOutInMS + "]";
            logger.warn(responseMessage);
            outputter.outputMessage(responseMessage);
        } catch (IOException e) {
            responseMessage = "Failed to connect to url [" + url
                    + "] with IOException [" + e.getMessage() + "]";
            logger.warn(responseMessage);
            outputter.outputMessage(responseMessage);
        }
    }

    public int getPollingIntervalInMS() {
        return pollingIntervalInMS;
    }
}
