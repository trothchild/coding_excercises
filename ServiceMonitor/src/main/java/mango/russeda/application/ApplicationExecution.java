package mango.russeda.application;

import java.io.IOException;

import mango.russeda.connectionchecker.MalformedEndPointException;
import mango.russeda.filehandling.LoadServicesToCheckFromFile;
import mango.russeda.output.Output;
import mango.russeda.threading.ConnectionCheckerScheduler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ApplicationExecution {

    private static int runTimeInSeconds;
    private static final Logger logger = LoggerFactory
            .getLogger(ApplicationExecution.class);

    /**
     * This method will load configuration from Spring-Config.xml in the
     * classpath The default of which will load services from config.ini in the
     * in the format Name, endpoint, polling interval (in milliseconds) and
     * output failure of services to System.out
     * 
     * @param args
     *            Only accepted parameter is an optional run duration in seconds
     * @throws NumberFormatException
     * @throws Exception
     * @throws MalformedEndPointException
     */
    public static void main(String[] args) throws NumberFormatException,
            Exception, MalformedEndPointException {
        logger.info("Service monitor is starting");
        checkParameters(args);
        if (args.length == 1) {
            runTimeInSeconds = new Integer(args[0]);
        }
        wireEveryThingTogether();

    }

    private static void checkParameters(String[] args) {
        if (args == null || args.length > 1
                || ((args.length == 1) && !checkStringisInteger(args[0]))) {
            throw new IllegalArgumentException(
                    "Only parameter is an optional run duration in seconds");
        }
    }

    private static boolean checkStringisInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    private static void wireEveryThingTogether() throws NumberFormatException,
            IOException, MalformedEndPointException, InterruptedException {

        ApplicationContext context = new ClassPathXmlApplicationContext(
                "Spring-Config.xml");

        Output outputter = (Output) context.getBean("output");

        LoadServicesToCheckFromFile serviceLoader = (LoadServicesToCheckFromFile) context
                .getBean("LoadServicesToCheckFromFile");

        ConnectionCheckerScheduler scheduler = new ConnectionCheckerScheduler(
                serviceLoader.getConnectionCheckerTaskList(), context);
        scheduler.startScheduledTasks();
        String message;

        attachShutDownHook();
        if (runTimeInSeconds > 0) {
            message = "Service monitor has started and will operate for "
                    + runTimeInSeconds / 1000 + " seconds";
            logger.info(message);
            outputter.outputMessage(message);
            Thread.sleep(runTimeInSeconds);
        } else {
            message = "Service monitor has started and will operate until terminated";
            outputter.outputMessage(message);
            logger.info(message);
            while (true)
                ;
        }

        System.exit(0);

    }

    public static void attachShutDownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread() {

            ApplicationContext context = new ClassPathXmlApplicationContext(
                    "Spring-Config.xml");

            @Override
            public void run() {
                logger.info("Shut down hook activated");
                Output outputter = (Output) context.getBean("output");
                String message = "Service monitor has finished";
                logger.info(message);
                // System.out.println(message);
                outputter.outputMessage(message);
            }
        });
        logger.info("Shut Down Hook Attached.");
    }

}
