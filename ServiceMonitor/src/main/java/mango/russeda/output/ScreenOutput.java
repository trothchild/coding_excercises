package mango.russeda.output;

public class ScreenOutput implements Output {

    public void outputMessage(String message) {
        checkParamters(message);
        System.out.println(message);
    }

    private void checkParamters(String message) {
        if (message == null) {
            throw new IllegalArgumentException("Output message can not be null");
        }

    }

}
