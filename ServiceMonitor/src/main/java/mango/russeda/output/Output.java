package mango.russeda.output;

public interface Output {
    public void outputMessage(String message);
}
