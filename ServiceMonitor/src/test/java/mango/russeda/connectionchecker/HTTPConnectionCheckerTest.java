package mango.russeda.connectionchecker;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;

import mango.russeda.output.Output;
import mango.russeda.output.ScreenOutput;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(value = { HTTPConnectionChecker.class })
public class HTTPConnectionCheckerTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final int noTimeOut = 0;
    private final int defaultTimeOut = 500;
    private final String endPoint = "http://google.com";
    private Output screenOutput;
    private String name = "google";
    private int pollingIntervalInMS = 1000;
    private URL mockedUrl;
    private HttpURLConnection mockedHttpURLConnection;

    @Before
    public void setUp() throws Exception {
        mockedUrl = PowerMockito.mock(URL.class);
        mockedHttpURLConnection = mock(HttpURLConnection.class);
        PowerMockito.when(mockedUrl.openConnection()).thenReturn(
                mockedHttpURLConnection);
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
        // TODO potentially replace with mock
        screenOutput = new ScreenOutput();

    }

    @After
    public void tearDown() {
        System.setOut(null);
        System.setErr(null);
    }

    // replace real connection with mocks
    @Test(timeout = 3000)
    public void testRealConnection() throws IOException,
            MalformedEndPointException {

        HTTPConnectionChecker myChecker = new HTTPConnectionChecker(name,
                endPoint, pollingIntervalInMS, noTimeOut, screenOutput);

        myChecker.checkConnection();
    }

    @Test
    public void testGetPollingInterval() throws IOException,
            MalformedEndPointException {

        HTTPConnectionChecker myChecker = new HTTPConnectionChecker(name,
                endPoint, pollingIntervalInMS, defaultTimeOut, screenOutput,
                mockedUrl);

        assertEquals(pollingIntervalInMS, myChecker.getPollingIntervalInMS());

        verifyNoMoreInteractions(mockedHttpURLConnection);
    }

    @Test(timeout = 3000)
    public void testResult200() throws IOException, MalformedEndPointException {
        int responseCode = 200;
        String expectedResponse = "";
        when(mockedHttpURLConnection.getResponseCode())
                .thenReturn(responseCode);

        HTTPConnectionChecker myChecker = new HTTPConnectionChecker(name,
                endPoint, pollingIntervalInMS, noTimeOut, screenOutput,
                mockedUrl);

        myChecker.checkConnection();
        verify(mockedHttpURLConnection).setRequestMethod("HEAD");
        verify(mockedHttpURLConnection).getResponseCode();
        assertEquals(expectedResponse, outContent.toString());
    }

    @Test(timeout = 3000)
    public void testResult300() throws IOException, MalformedEndPointException {
        int responseCode = 300;
        String expectedResponse = "An unsuccessful response[" + responseCode
                + "] was received from end point [" + mockedUrl + "]"
                + System.getProperty("line.separator");
        when(mockedHttpURLConnection.getResponseCode())
                .thenReturn(responseCode);
        HTTPConnectionChecker myChecker = new HTTPConnectionChecker(name,
                endPoint, pollingIntervalInMS, noTimeOut, screenOutput,
                mockedUrl);

        myChecker.checkConnection();
        assertEquals(expectedResponse, outContent.toString());
    }

    @Test
    public void testResultDelayed() throws InterruptedException, IOException,
            MalformedEndPointException {
        String expectedResponse = "";

        Answer<Integer> answer = new Answer<Integer>() {
            public Integer answer(InvocationOnMock invocation) throws Throwable {
                Thread.sleep(defaultTimeOut);
                return 200;
            }
        };

        when(mockedHttpURLConnection.getResponseCode()).thenAnswer(answer);

        HTTPConnectionChecker myChecker = new HTTPConnectionChecker(name,
                endPoint, pollingIntervalInMS, defaultTimeOut, screenOutput,
                mockedUrl);

        myChecker.checkConnection();
        assertEquals(expectedResponse, outContent.toString());
    }

    @Test(timeout = 3000)
    public void testResultTimeout() throws IOException,
            MalformedEndPointException {
        PowerMockito.when(mockedUrl.toString()).thenReturn("mockedUrl");
        String expectedResponse = "A response from [" + mockedUrl
                + "] was not received in time [" + defaultTimeOut + "]"
                + System.getProperty("line.separator");

        when(mockedHttpURLConnection.getResponseCode()).thenThrow(
                new java.net.SocketTimeoutException());

        HTTPConnectionChecker myChecker = new HTTPConnectionChecker(name,
                endPoint, pollingIntervalInMS, defaultTimeOut, screenOutput,
                mockedUrl);

        myChecker.checkConnection();
        assertEquals(expectedResponse, outContent.toString());
    }

    @Test(timeout = 3000)
    public void testExceptionThrown() throws IOException,
            MalformedEndPointException {
        PowerMockito.when(mockedUrl.toString()).thenReturn("mockedUrl");

        IOException thrownIOException = new IOException("I/O error occured");
        String expectedResponse = "Failed to connect to url [" + mockedUrl
                + "] with IOException [" + thrownIOException.getMessage() + "]"
                + System.getProperty("line.separator");

        when(mockedHttpURLConnection.getResponseCode()).thenThrow(
                thrownIOException);

        HTTPConnectionChecker myChecker = new HTTPConnectionChecker(name,
                endPoint, pollingIntervalInMS, defaultTimeOut, screenOutput,
                mockedUrl);

        myChecker.checkConnection();
        assertEquals(expectedResponse, outContent.toString());
    }

    @Test(timeout = 3000)
    public void testNullOutputterConstructorWithURL()
            throws MalformedEndPointException {

        try {
            new HTTPConnectionChecker(name, endPoint, pollingIntervalInMS,
                    defaultTimeOut, null, mockedUrl);
            fail("Should not be able to create a http checker with a null output");
        } catch (IllegalArgumentException e) {
            assertEquals(
                    "Can not create a HTTPConnectionChecker with a null output",
                    e.getMessage());
        }
    }

    @Test(timeout = 3000)
    public void testNullOutputter() throws MalformedEndPointException {

        try {
            new HTTPConnectionChecker(name, endPoint, pollingIntervalInMS,
                    defaultTimeOut, null);
            fail("Should not be able to create a http checker with a null output");
        } catch (IllegalArgumentException e) {
            assertEquals(
                    "Can not create a HTTPConnectionChecker with a null output",
                    e.getMessage());
        }
    }
}
