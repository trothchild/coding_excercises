package mango.russeda.connectionchecker;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import mango.russeda.output.Output;
import mango.russeda.output.ScreenOutput;

import org.junit.Test;

public class ConnectionCheckerFactoryTest {

    private String endPoint = "http://www.google.com";
    private String name = "google";
    private int pollingIntervalInMS = 1000;
    private Output screenOutputter = new ScreenOutput();

    @Test
    public void testValidParameters() throws MalformedEndPointException {
        ConnectionChecker myConnectionChecker = ConnectionCheckerFactory
                .createConnectionChecker(name, endPoint, pollingIntervalInMS,
                        screenOutputter);

        assertEquals(myConnectionChecker.getClass(),
                HTTPConnectionChecker.class);
    }

    @Test
    public void testNullName() throws MalformedEndPointException {

        try {
            ConnectionCheckerFactory.createConnectionChecker(null, endPoint,
                    pollingIntervalInMS, screenOutputter);
            fail("SHould not be able to create a connection checker withoout a null url");
        } catch (NullPointerException e) {
            assertEquals(
                    "The name and URL of the connection checker must be populated",
                    e.getMessage());
        }
    }

    @Test(expected = NullPointerException.class)
    public void testNullURL() throws MalformedEndPointException {
        ConnectionCheckerFactory.createConnectionChecker(name, null,
                pollingIntervalInMS, screenOutputter);
    }

    @Test
    public void testNoName() throws MalformedEndPointException {
        String name = "";
        try {
            ConnectionCheckerFactory.createConnectionChecker(name, endPoint,
                    pollingIntervalInMS, screenOutputter);
            fail("Should not be able to create a connection checker without a valid name");
        } catch (IllegalArgumentException e) {
            assertEquals("The name used in connection checker must be set",
                    e.getMessage());
        }
    }

    @Test
    public void testNoEndPoint() throws MalformedEndPointException {
        String badEndPoint = "";
        try {
            ConnectionCheckerFactory.createConnectionChecker(name, badEndPoint,
                    pollingIntervalInMS, screenOutputter);
            fail("Should not be able to create a connection checker without a valid name");
        } catch (IllegalArgumentException e) {
            assertEquals(
                    "The end point used in connection checker must be set",
                    e.getMessage());
        }
    }

    @Test
    public void testNegativePollingInterval() throws MalformedEndPointException {
        int badPollingIntervalInMS = -1000;

        try {
            ConnectionCheckerFactory.createConnectionChecker(name, endPoint,
                    badPollingIntervalInMS, screenOutputter);
            fail("Should not be able to create a connection checker with a negative polling intervale");

        } catch (IllegalArgumentException e) {
            assertEquals("The polling interval must be greater than zero ["
                    + badPollingIntervalInMS + "]", e.getMessage());

        }
    }

    @Test
    public void testUnknownProtocol() throws MalformedEndPointException {

        int pollingIntervalInMS = 1000;
        String unknownEndPointProtocol = "ftp://www.google.com";
        String name = "google";
        try {
            ConnectionCheckerFactory.createConnectionChecker(name,
                    unknownEndPointProtocol, pollingIntervalInMS,
                    screenOutputter);
            fail("Should not be able to create a connection checker with an unknown protocol");
        } catch (IllegalArgumentException e) {
            assertEquals("Protocol is unsupported [" + unknownEndPointProtocol
                    + "]", e.getMessage());
        }
    }

}
