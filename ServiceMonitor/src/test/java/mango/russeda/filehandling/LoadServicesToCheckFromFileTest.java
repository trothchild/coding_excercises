package mango.russeda.filehandling;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import mango.russeda.connectionchecker.MalformedEndPointException;
import mango.russeda.output.Output;
import mango.russeda.output.ScreenOutput;
import mango.russeda.threading.ConnectionCheckerTask;

import org.junit.Before;
import org.junit.Test;

public class LoadServicesToCheckFromFileTest {

    private Output defaultOutputter;

    @Before
    public void setUp() throws Exception {
        defaultOutputter = new ScreenOutput();
    }

    @Test
    public void testEmptyFileName() throws Exception {
        String fileName = "";
        try {
            new LoadServicesToCheckFromFile(fileName, defaultOutputter);
            fail("Should not be able to create class with empty filename");
        } catch (IllegalArgumentException e) {
            assertEquals("Filename can not be empty", e.getMessage());
        }
    }

    @Test
    public void testNullFileName() throws Exception {
        try {
            new LoadServicesToCheckFromFile(null, defaultOutputter);
            fail("Should not be able to create class with null filename");
        } catch (IllegalArgumentException e) {
            assertEquals("Filename can not be null", e.getMessage());
        }
    }

    @Test
    public void testNullOutputter() throws Exception {
        String validFile = "src/test/resources/EmptyFile.txt";
        try {
            new LoadServicesToCheckFromFile(validFile, null);
            fail("Should not be able to create class with null outputter");
        } catch (IllegalArgumentException e) {
            assertEquals("Outputter can not be null", e.getMessage());
        }
    }

    @Test
    public void testFileDoesNotExist() throws Exception {
        String fileDoesNotExist = "...hadock";
        try {
            new LoadServicesToCheckFromFile(fileDoesNotExist, defaultOutputter);
            fail("Should not be able to create with a filename that doesn't exist");
        } catch (FileNotFoundException e) {
            assertEquals("File not found [" + fileDoesNotExist + "]",
                    e.getMessage());
        }
    }

    @Test
    public void testFileNameThatIsADirectory() throws Exception {
        String directoryName = "src/test/resources";

        try {
            new LoadServicesToCheckFromFile(directoryName, defaultOutputter);
            fail("Should not be able to create with a filename that is a directory");
        } catch (IllegalArgumentException e) {
            assertEquals("Filename is a directory [src/test/resources]",
                    e.getMessage());
        }
    }

    @Test
    public void testLoadingEmptyFile() throws IOException,
            NumberFormatException, MalformedEndPointException {
        String emptyFile = "src/test/resources/EmptyFile.txt";
        LoadServicesToCheckFromFile myLoader = new LoadServicesToCheckFromFile(
                emptyFile, defaultOutputter);
        List<ConnectionCheckerTask> checkTasks = myLoader
                .getConnectionCheckerTaskList();
        assertEquals(0, checkTasks.size());
    }

    @Test
    public void testLoadValidFile() throws IOException, NumberFormatException,
            MalformedEndPointException {
        String singleServiceFile = "src/test/resources/SingleService.txt";
        LoadServicesToCheckFromFile myLoader = new LoadServicesToCheckFromFile(
                singleServiceFile, defaultOutputter);
        List<ConnectionCheckerTask> checkTasks = myLoader
                .getConnectionCheckerTaskList();
        assertEquals(1, checkTasks.size());
    }

    @Test
    public void testLoadValidFileWithTwo() throws IOException,
            NumberFormatException, MalformedEndPointException {
        String singleServiceFile = "src/test/resources/TwoServices.txt";
        LoadServicesToCheckFromFile myLoader = new LoadServicesToCheckFromFile(
                singleServiceFile, defaultOutputter);
        List<ConnectionCheckerTask> checkTasks = myLoader
                .getConnectionCheckerTaskList();
        assertEquals(2, checkTasks.size());
    }

    @Test
    public void testInvalidFileMissingName() throws Exception {
        String singleServiceFile = "src/test/resources/MissingNameField.txt";

        try {
            new LoadServicesToCheckFromFile(singleServiceFile, defaultOutputter);
            fail("Should not be able to load an invalid file");
        } catch (IllegalArgumentException e) {
            assertEquals("Unable to load file, please check logs",
                    e.getMessage());
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidFileMissingEndPoint() throws Exception {
        String singleServiceFile = "src/test/resources/MissingEndPointField.txt";
        new LoadServicesToCheckFromFile(singleServiceFile, defaultOutputter);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidFileMissingDelay() throws Exception {
        String singleServiceFile = "src/test/resources/MissingDelayField.txt";
        new LoadServicesToCheckFromFile(singleServiceFile, defaultOutputter);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidFileDelayNotNumber() throws Exception {
        String singleServiceFile = "src/test/resources/InvalidDelayField.txt";
        new LoadServicesToCheckFromFile(singleServiceFile, defaultOutputter);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testLessThanThreeParameters() throws Exception {
        String singleServiceFile = "src/test/resources/TwoArguments.txt";
        new LoadServicesToCheckFromFile(singleServiceFile, defaultOutputter);
    }

    @Test
    public void testFourArguments() throws IOException, NumberFormatException,
            MalformedEndPointException {
        String singleServiceFile = "src/test/resources/FourArguments.txt";
        LoadServicesToCheckFromFile myLoader = new LoadServicesToCheckFromFile(
                singleServiceFile, defaultOutputter);
        List<ConnectionCheckerTask> checkTasks = myLoader
                .getConnectionCheckerTaskList();
        assertEquals(1, checkTasks.size());
    }
}
