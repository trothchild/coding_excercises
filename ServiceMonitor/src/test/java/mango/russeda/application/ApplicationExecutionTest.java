package mango.russeda.application;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

public class ApplicationExecutionTest {

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testStartApplicationWithNullArg() throws Exception {
        try {
            ApplicationExecution.main(null);
            fail("Should not be able to start application with no args");
        } catch (IllegalArgumentException e) {
            assertEquals(
                    "Only parameter is an optional run duration in seconds",
                    e.getMessage());
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testStartApplicationWithThreeParameters() throws Exception {
        String[] args = { "1", "2", "3" };
        ApplicationExecution.main(args);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testStartApplicationWithTwoParameters() throws Exception {
        String[] args = { "1", "2" };
        ApplicationExecution.main(args);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testStartApplicationWithInvalidDuration() throws Exception {
        String[] args = { "FISHY" };
        ApplicationExecution.main(args);
    }

    // @Test
    public void testStartWithNoParameters() throws Exception {
        String[] args = {};
        ApplicationExecution.main(args);

    }

    // @Test
    public void testStartApplicationWithDuration() throws Exception {
        String[] args = { "1" };
        ApplicationExecution.main(args);
        Thread.sleep(1001);
    }
}
