package mango.russeda.threading;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import mango.russeda.connectionchecker.ConnectionChecker;

import org.junit.Test;

public class ConnectionCheckerTaskTest {

    @Test
    public void testTaskCallsConnection() {
        ConnectionChecker mockedChecker = mock(ConnectionChecker.class);
        ConnectionCheckerTask myTask = new ConnectionCheckerTask(mockedChecker);

        myTask.run();
        verify(mockedChecker).checkConnection();
    }

    @Test
    public void testTaskGetTimeout() {
        int pollingIntervalInMS = 1000;
        ConnectionChecker mockedChecker = mock(ConnectionChecker.class);
        when(mockedChecker.getPollingIntervalInMS()).thenReturn(
                pollingIntervalInMS);

        ConnectionCheckerTask myTask = new ConnectionCheckerTask(mockedChecker);

        assertEquals(pollingIntervalInMS, myTask.getPollingIntervalInMS());
        verify(mockedChecker).getPollingIntervalInMS();
        verifyNoMoreInteractions(mockedChecker);
    }

}
