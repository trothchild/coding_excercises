package mango.russeda.threading;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ConnectionCheckerSchedulerTest {

    private ApplicationContext context;
    private ConnectionCheckerTask mockedTask;
    private List<ConnectionCheckerTask> checkers;

    @Before
    public void setUp() throws Exception {
        context = new ClassPathXmlApplicationContext("Spring-Config.xml");
        mockedTask = mock(ConnectionCheckerTask.class);
        checkers = new ArrayList<ConnectionCheckerTask>();
        checkers.add(mockedTask);
    }

    @Test
    public void testSingleTask() throws InterruptedException {

        when(mockedTask.getPollingIntervalInMS()).thenReturn(1);

        ConnectionCheckerScheduler checkerSchedulerTest = new ConnectionCheckerScheduler(
                checkers, context);

        checkerSchedulerTest.startScheduledTasks();
        Thread.sleep(11);
        verify(mockedTask, atLeastOnce()).run();
    }

    @Test
    public void testSingleTaskMultipleTimes() throws InterruptedException {

        when(mockedTask.getPollingIntervalInMS()).thenReturn(1);

        ConnectionCheckerScheduler checkerSchedulerTest = new ConnectionCheckerScheduler(
                checkers, context);

        checkerSchedulerTest.startScheduledTasks();

        // short sleep to allow multiple schedules to run
        Thread.sleep(40);

        verify(mockedTask, atLeastOnce()).run();
    }

    @Test
    public void testTwoTask() throws InterruptedException {

        when(mockedTask.getPollingIntervalInMS()).thenReturn(11);

        ConnectionCheckerTask mockedTask2 = mock(ConnectionCheckerTask.class);
        when(mockedTask2.getPollingIntervalInMS()).thenReturn(50);

        checkers.add(mockedTask2);

        ConnectionCheckerScheduler checkerSchedulerTest = new ConnectionCheckerScheduler(
                checkers, context);

        checkerSchedulerTest.startScheduledTasks();

        // short sleep to allow multiple schedules to run
        Thread.sleep(40);

        verify(mockedTask2, atLeastOnce()).run();
        verify(mockedTask, atLeastOnce()).run();
    }

    @Test
    public void testNullTaskArray() {

        try {
            new ConnectionCheckerScheduler(null, context);
            fail("Should not be able to create a scheduler with null check connection task array");
        } catch (IllegalArgumentException e) {
            assertEquals("The check connection task array should not be null",
                    e.getMessage());
        }
    }

    @Test
    public void testNullContext() {

        try {
            new ConnectionCheckerScheduler(checkers, null);
            fail("Should not be able to create a scheduler with null check connection task array");
        } catch (IllegalArgumentException e) {
            assertEquals("The spring context should not be  null",
                    e.getMessage());
        }
    }

    @Test
    public void testEmptyConnectionCheckerTaskArray() {
        new ConnectionCheckerScheduler(checkers, context);
    }
}
