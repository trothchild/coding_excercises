package mango.russeda.output;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ScreenOutputTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final int noTimeOut = 0;
    private ScreenOutput screenOutput;

    @Before
    public void setUp() throws Exception {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
        screenOutput = new ScreenOutput();
    }

    @After
    public void tearDown() {
        System.setOut(null);
        System.setErr(null);
    }

    @Test
    public void testMessage() {
        String message = "Test message";
        String expectedOutput = message + System.getProperty("line.separator");

        screenOutput.outputMessage(message);
        assertEquals(expectedOutput, outContent.toString());
    }

    @Test
    public void testNullMessage() {
        try {
            screenOutput.outputMessage(null);
            fail("Should not be able to output a null message");
        } catch (IllegalArgumentException e) {
            assertEquals("Output message can not be null", e.getMessage());
        }
    }

    @Test
    public void testEmptyMessage() {
        String expectedOutput = System.getProperty("line.separator");
        screenOutput.outputMessage("");
        assertEquals(expectedOutput, outContent.toString());
    }
}